
_fix_postgrey_db() {

echo "ACTION::"${FUNCNAME[0]}
##     NO service executable
echo "<<<<< STOP <<<<"$(date +%H:%M:%S)
which service 2>/dev/null|wc -l|grep -q ^0$ && /etc/init.d/postgrey stop 2>&1 |grep -v -e Resolved -e sender= -e client_address=
## HAVING service executable
which service 2>/dev/null|wc -l|grep -q ^0$ || service postgrey stop 2>&1 |grep -v -e Resolved -e sender= -e client_address=
echo "<<<<< STOP <<<<"$(date +%H:%M:%S)

echo "~~~~~~ FIX ~~~~~~>"$(date +%H:%M:%S)
echo "DELETING POSTGREY_DB"
find /var/lib/postgrey -type f -print -delete |sed 's/$/ /g'|tr -d '\n'
echo "~~~~~~ FIX ~~~~~~<"$(date +%H:%M:%S)

echo ">>>>>> START >>>>"$(date +%H:%M:%S)
##     NO service executable
which service 2>/dev/null|wc -l|grep -q ^0$ && /etc/init.d/postgrey start 2>&1 |grep -v -e Resolved -e sender= -e client_address=
## HAVING service executable
which service 2>/dev/null|wc -l|grep -q ^0$ || service postgrey start 2>&1 |grep -v -e Resolved -e sender= -e client_address=
echo ">>>>>> START >>>>"$(date +%H:%M:%S)
##     NO service executable
which service 2>/dev/null|wc -l|grep -q ^0$ && /etc/init.d/postgrey status 2>&1 |grep -v -e Resolved -e sender= -e client_address=
## HAVING service executable
which service 2>/dev/null|wc -l|grep -q ^0$ || service postgrey status  2>&1 |grep -v -e Resolved -e sender= -e client_address=
echo ;   } ;

_restart_postgrey() {
 echo ">>>>>> START >>>>"$(date +%H:%M:%S)
##     NO service executable
which service 2>/dev/null|wc -l|grep -q ^0$ && /etc/init.d/postgrey restart 2>&1 |grep -v -e Resolved -e sender= -e client_address=
## HAVING service executable
which service 2>/dev/null|wc -l|grep -q ^0$ || service postgrey restart 2>&1 |grep -v -e Resolved -e sender= -e client_address=
echo ">>>>>> START >>>>"$(date +%H:%M:%S)
##     NO service executable
which service 2>/dev/null|wc -l|grep -q ^0$ && /etc/init.d/postgrey status 2>&1 |grep -v -e Resolved -e sender= -e client_address=
## HAVING service executable
which service 2>/dev/null|wc -l|grep -q ^0$ || service postgrey status  2>&1 |grep -v -e Resolved -e sender= -e client_address=


  echo -n  ; } ;
