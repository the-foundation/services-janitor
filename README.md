Daemon Janitor
==============

**Watchdog / Janitor** for services to be **watched** and **restarted** after certain **repair actions**

* **searches for a string in specified logs for the last n minutes (15 default )**
* **notfication via telegram**
* **automatic actions on service failure**
---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/services-janitor/README.md/logo.jpg" width="480" height="270"/></div></a>
