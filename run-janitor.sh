#!/bin/bash


fn_exists() {    LC_ALL=C type $1 2>&1 | grep -q -e 'shell function' -e 'is a function' ; } ;
__libnotfound() { echo "NO LIBRARY FOUND exiting" >&2 ;exit 666   ;} ;

echo -n ${SCRIPT} |wc -c |grep -vq ^0$ && MYPATH=$(dirname $SCRIPT)
if [ -z "$MYPATH" ];then MYPATH=/etc/custom/services-janitor;fi
test -f ${MYPATH}/lib_services-janitor_fixes.sh || __libnotfound
. ${MYPATH}/lib_services-janitor_fixes.sh

message_buffer=""
backlog_minutes=15;

searchterms() {
  for searchstamp in $(seq 0 60 $((60*backlog_minutes)));do
   LANG=en_US.UTF-8 date "+%b %d %H:%M" -d @"$(($(date +%s)-${searchstamp}))";done ;
  } ;
greptarget=$(searchterms|sed 's/^/-e "/g;s/$/"/g' )

_log_get_syslog() {
  ##Openwrt
  which logread 2>/dev/null|wc -l|grep -qv ^0$ &&  ( grepcmd=$(echo  grep '-e' "$greptarget"); echo "logread| $(echo $grepcmd)"|sed 's/-e -e/-e/g'|sh )
  ## Linux Debian/Ubuntu
  which logread 2>/dev/null|wc -l|grep -q  ^0$ &&  ( echo grep '-e' $greptarget /var/log/syslog|sed 's/-e -e/-e/g'|sh )
 } ;

_log_get_maillog() {
  test -f /var/log/mail.log  && (echo grep  $greptarget /var/log/mail.log|sh )
  } ;

##base64 per line buffer
#mail_logbuf=$(_log_get_maillog|while read line;do echo $line|base64 |tr -d '\n';echo  ;done)
##mail log buffer
mail_logbuf=$(_log_get_maillog)
sys_logbuf=$(_log_get_syslog)

fix_functions=""
IFSREAL=$IFS;IFS=$'\n';for errorline in $(cat errors.csv|grep -v ^$) ;do
    failmsg64=$(echo "$errorline"|cut -f1|base64 |tr -d '\n' );
    current_errors=$(echo   "${sys_logbuf}"|grep "$(echo "$failmsg64" |base64 -d)" |grep -v ^$ ) ;
    current_errors=$(echo  "${mail_logbuf}"|grep "$(echo "$failmsg64" |base64 -d)" |grep -v ^$ ) ;
    echo "${current_errors}"|grep -v ^$|wc -l |grep -q -v ^0$ && fix_functions=${fix_functions}" "$(echo "$errorline"|cut -f3 );
    message_buffer=${message_buffer}" "$(echo "::ERR:" ${current_errors})
done

## scroll through errors.csv, get base64 version of first field ( tab separated)
## search the corresponding string in out log window
IFS=$IFSREAL
for function_to_call in $(echo $fix_functions|sed 's/ /\n/g'|sort -u );do
  ##echo searching function _"$function_to_call"
### ATTENTION: function name in config csv without underscore
  fn_exists _"$function_to_call" && message_buffer="${message_buffer} "$(echo "###############";echo "::AUTOFIX $function_to_call";echo "---------------"; )
  message_buffer="${message_buffer} "$(_$function_to_call)" "$(echo;echo "--------------------------")
done


### FIX THINGIES
message_buffer=$(echo "${message_buffer}"|sed 's/ ::\(ERR\|AUTOFIX\)/\n\0/g' |grep -v "^ ::ERR:$")
##telegram-notify dislikes underscores
MSGTXT="${message_buffer//_/-}"

echo $message_buffer|sed 's/ \+/ /g'|grep -e -q  -e "^ $"  -e "^$" || (
  which telegram-notify 2>/dev/null|wc -l|grep -qv ^0$ && ( echo "$MSGTXT" | telegram-notify --config /etc/telegram-notify.conf --silent --text - 2>&1 |grep -v "Using configuration file"  )
  which telegram-notify 2>/dev/null|wc -l|grep -q  ^0$ && ( echo "${message_buffer}" | sendmail root@$(hostname -f) )
)
